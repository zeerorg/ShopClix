package com.shopclix.rishabh.shopclix;

/**
 * Created by rishabh on 2/25/17.
 */

public class User {
    public String name;
    public String email;
    public String phone;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    public User(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
